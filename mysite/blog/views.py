from django.shortcuts import render, get_object_or_404, redirect
from blog.models import Post
from django.utils import timezone
from .forms import PostForm


def post_list(request):
    '''
    For listing all the post
    '''

    posts = \
        Post.objects.filter(published_date__lte=timezone.now()).\
        order_by('-published_date')
    return render(request, 'blog/post_list.html', {'posts': posts})


def post_detail(request, pk_id):
    '''
    For showing detail of the post
    '''

    post = get_object_or_404(Post, pk=pk_id)
    return render(request, 'blog/post_detail.html', {'post': post})


def post_new(request):
    '''
    Add new post
    '''

    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk_id=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_new.html', {'form': form})
