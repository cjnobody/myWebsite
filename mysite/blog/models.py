from django.db import models
from django.utils import timezone

class Post(models.Model):

    class Meta:
        db_table = 'blog_post'

    title = models.CharField(max_length=255, blank=True, null=True)
    content = models.TextField()
    create_date = models.DateTimeField(auto_now=True)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
